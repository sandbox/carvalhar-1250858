This module mix Rules and User Forced Term, creating a new action to be used with Rules.
This action has two arguments, a term and a user, both configurable by rules. 

Project Page:
http://drupal.org/project/user_force_term
http://drupal.org/project/rules
